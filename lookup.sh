#!/bin/bash

#Usage - sh lookup.sh domain/ip/hostname vpn_ip_address
print_details()
{
  echo "
  ====
  Domain : $domain
  IP     : $ip
  PTR    : $rdns
  VPN IP : $vpn_ip
  ====
  ";
}
 
 
do_telnet_check()
{
  timeout 5 telnet $1 587 
}
 
 
process_plesk_details()
{
  windash=`echo $rdns | sed "s|.webhostbox.net|.internal.webhostbox.net/windash|g"`
  db="sdh"
  server_type=`echo $rdns | awk -F ".webhostbox.net" '{print $1}'`
  is_bh=`echo $server_type | grep -i bh | wc -l `
  if [ $is_bh = "1" ]
  then
        db="rh"
  fi
 
  is_mdh=`echo $server_type | grep -i md | wc -l `
  if [ $is_mdh = "1" ]
  then
        db="mdh"
  fi
 
  echo "Windash: $windash"; echo ""
  echo "Generating Plesk link for $db package..."
 ssh -i "/home/muhammed.k/.ssh/techsupp_key" -tqo StrictHostKeyChecking=no techsupp@10.86.210.2 "python2.6 /usr/local/getSSOUrl-supportadmin servername=$rdns ip=$vpn_ip type=$db"
#su -c cpsessionmanager techsupp

}
 

process_if_url()
{
 url=`echo $domain | egrep -i "http:|https:" | wc -l`
 
 if [ $url == 1 ]
 then
   domain=`echo $domain| sed "s,/$,,"`
   domain=`echo $domain | sed "s|^http://||" `
   domain=`echo $domain | sed "s|^https://||" `
 fi
}
 
 
 
process_input()
{
   if [ -z "$domain" ]
    then
    read -p "Domain: " domain;
  fi
  process_if_url
}
 

domain=$1;
vpn_ip=$2;
process_input;
if [[ $1 =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
 ip=$1
else
 ip=`dig +short $domain| tail -1`;
fi

rdns_chk=`echo $1 | grep -i webhostbox.net | wc -l`;
if [ $rdns_chk == "0" ]
then
  rdns=`dig -x $ip +short | tail -1|sed "s|\.$||"`;
  rdns=${rdns,,}
  our_rdns=`echo $rdns | grep -i webhostbox.net | wc -l`;
  if [ $our_rdns == "0" ]; then
   rdns=$(timeout --foreground 3 telnet $ip 587 | grep -i webhostbox.net|cut -d" " -f2)
   rdns=${rdns,,}
  fi
else
  rdns=${1,,}
fi

our_rdns=`echo $rdns | grep -i webhostbox.net | wc -l`;
if [ $our_rdns == "0" ] 
then
  print_details
  echo -e "\n Error in finding hostname !!\n"
#  do_telnet_check $ip
else
  print_details
  process_plesk_details
fi
